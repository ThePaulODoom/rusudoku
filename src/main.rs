mod sudoku;

use std::env::args;
use std::process::exit;
use sudoku::*;

fn print_usage(pgrm_name: &str) {
    println!("\tusage: {} <board> \n
    Rusudoku Copyright (C) 2020 ThePaulODoom
    This program comes with ABSOLUTELY NO WARRANTY
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details. ",pgrm_name);
    exit(1);
}


fn main() {
    let arg: Vec<String> = args().collect();
    if arg.len() != 2 { print_usage(&arg[0]); }
    let board = gen_board(&arg[1]);
    let newboard = solve(Some(&board));
    print_board(&newboard.unwrap());
}
