pub  fn print_board(board: &[[u8;9];9]) {
    for i in board.iter() {
        for j in i.iter() {
            print!("{} ", j);
        }
        print!("\n");
    }
}

pub  fn gen_board(brdstr: &str) -> [[u8;9];9]{
    let mut board: [[u8;9];9] = [[0;9];9];
    for i in 0..9 {
        for j in 0..9 {
            board[i][j] = brdstr.chars().map(|c| c.to_digit(10).unwrap()).nth((i*9)+j).unwrap() as u8;
        }
    }
    board
}

pub  fn find_new(board: &[[u8;9];9]) -> Option<(usize,usize)> {
    for i in 0..9usize {
        for j in 0..9usize {
            if board[i][j] == 0 {
                return Some((i,j));
            }
        }
    }
    return None;
}

pub  fn check(board: &[[u8;9];9], pos: (usize, usize), number: u8) -> bool {
    // rows
    for i in 0..9 {
        if number == board[pos.0][i] { return false; }
    }

    // colomns
    for i in 0..9 {
        if number == board[i][pos.1] { return false; }
    }

    // squares
    let starti = pos.0 / 3;
    let startj = pos.1 / 3;

    for i in (starti * 3)..((starti * 3) + 3) {
        for j in (startj * 3)..((startj * 3) + 3) {
            if board[i][j] == number {
                return false;
            }
        }
    }
    return true;
}

pub  fn solve(board: Option<&[[u8;9];9]>) -> Option<[[u8;9];9]> {
    let mut newb = board.unwrap().clone();

    let (y,x) = match find_new(&newb) {
        Some(coord) => coord,
        None => return Some(newb),
    };

    for i in 1..10 {
        if check(&newb, (y,x), i) {
            newb[y][x] = i;
            match solve(Some(&newb)) {
                Some(x) => return Some(x),
                None => newb[y][x] = i,
            }
        }
    }

    return None;
}
