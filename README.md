# Rusudoku
## About Rusudoku
Rusudoku is a sudoku solver written in Rust. The name is derived from Rust and sudoku.
## How do I use it?
You use it like `rusudoku <board>` where `<board>` is a string in the BOARD STRINGS section
### BOARD STRINGS
A board like this
```5 6 0 0 0 8 0 3 9
0 0 1 0 0 2 7 0 6
2 7 9 4 0 0 1 8 0
0 0 0 0 2 0 6 0 0
0 5 0 0 0 0 0 9 0
0 0 6 0 7 0 0 0 0
0 1 2 0 0 5 9 7 3
9 0 5 2 0 0 8 0 0
7 3 0 1 0 0 0 2 4
```
Would be encoded like this `560008039001002706279400180000020600050000090006070000012005973905200800730100024`
where the `0`s would be empty spaces
